if (!$().getAuthToken()) {
  $.fn.redirectToLogin() // TODO check validity of token
}

$(document).ready(() => {
  $('.menu .item').tab({
    history: true,
    cache: false
  });

  $('#logout').click(() => {
    $().logout();
  });

  $.fn.loadAccountDataIntoForm = (cb) => {
    $('form#account-data-form').api({
      on: 'now',
      url: '/api/users/me',
      method: 'GET',
      serializeForm: false,
      onSuccess: (response, element, xhr) => {
        if (cb) cb(response);
        $(element).form('set values', $().convertToRetardedFormObject(response))
      },
      onFailure: (response) => {
        if (response.statusCode === 401) $().logout();
      }
    });
  };
  $.fn.editUser = id => {
    $('#edit-user-modal form').api({
      on: 'now',
      url: '/api/users/' + id,
      method: 'GET',
      serializeForm: false,
      onSuccess: (response, element, xhr) => {
        $(element).form('set values', $().convertToRetardedFormObject(response));
        $('#edit-user-modal').modal('show')
      }
    });
  };
  $.fn.deleteUser = id => {
    $('html').api({
      url: '/api/users/' + id,
      method: 'DELETE',
      on: 'now',
      onSuccess: () => {
        $().loadUsers();
      }
    });
  };
  $.fn.loadUsers = () => {
    const limit = 25;
    $('#users-table').api({
      on: 'now',
      url: '/api/users?limit=' + limit + '&start=' + (Number($('#current-user-page').attr('data-value'))-1)*limit,
      method: 'GET',
      serializeForm: false,
      onSuccess: (response, element, xhr) => {
        const template = document.querySelector('#users-table template');
        const target = $('#users-table tbody');
        target.html('');
        response.forEach(user => {
          let clone = document.importNode(template.content, true);
          let td = clone.querySelectorAll('td');
          td[0].textContent = user.id;
          td[1].textContent = user.email;
          td[2].textContent = user.funk ? user.funk.username : '';
          td[3].textContent = user.isAdmin ? 'yes' : 'no';
          let buttons = clone.querySelectorAll('.button');
          $(buttons[0]).click(event => {
            $().editUser(user.id);
          });
          $(buttons[1]).click(event => {
            $().deleteUser(user.id);
          });
          target.append(clone);
        });
      }
    });
  };
  $().loadAccountDataIntoForm(res => {
    if (!res.isAdmin) return;
    $('.menu [data-tab=\'users\']').css('display', '');
    $().loadUsers();
  });

  $('#users-table .menu .icon.item').click(event => {
    const change = $(event.target).attr('data-action') === 'increase' ? 1 : -1;
    const pageElement = $('#current-user-page');
    let currentPage = Number(pageElement.attr('data-value'));
    if (currentPage === 1 && change === -1) return;
    currentPage += change;
    pageElement.attr('data-value', currentPage);
    pageElement.text(currentPage);
    $().loadUsers();
  });

  $('form#account-data-form .button.cancel').click(event => {
    $(event.target.parentElement).form('clear');
    $().loadAccountDataIntoForm();
  });

  $('form#account-data-form .button.submit')
    .form({
      on: 'blur',
      inline: true,
      fields: {
        email: {
          identifier: 'email',
          rules: [
            {
              type: 'empty',
              prompt: 'Please enter your email'
            },
            {
              type: 'email',
              prompt: 'Please enter a valid email'
            }
          ]
        },
        funkusername: {
          identifier: 'funk[username]',
          rules: [
            {
              type: 'empty',
              prompt: 'Please enter your Funk username'
            }
          ]
        }
      }
    })
    .api({
      url: '/api/users/me',
      method: 'POST',
      beforeSend: function (settings) {
        if (!settings.data.funk.password) delete settings.data.funk.password;
        Object.entries(settings.data.schedule).forEach(([k, v]) => {
          settings.data.schedule[k] = Number(v);
        }); // Convert strings to numbers
        return $.fn.api.settings.beforeSend(settings);
      }
    });

  $('#password-modal form')
    .form({
      on: 'blur',
      inline: true,
      fields: {
        oldPassword: {
          rules: [
            {
              type: 'empty',
              prompt: 'Please enter your old password'
            }
          ]
        },
        newPassword: {
          rules: [
            {
              type: 'empty',
              prompt: 'Please enter a new password'
            }
          ]
        },
        newPasswordRepeat: {
          rules: [
            {
              type: 'match[newPassword]',
              prompt: 'The two passwords don\'t match'
            }
          ]
        }
      }
    });

  $('.ui.modal#password-modal').modal({
    onApprove: function (event) {
      let form = $('#password-modal form');
      form.form('validate form');
      if (!form.form('is valid')) return false;
      form.api({
        url: '/api/users/me/password',
        method: 'POST',
        on: 'now',
        beforeSend: function (settings) {
          settings.data = {old: settings.data.oldPassword, new: settings.data.newPassword};
          return $.fn.api.settings.beforeSend(settings);
        },
        onSuccess: (response, element, xhr) => {
          $('.ui.modal#password-modal').modal('hide');
        } //TODO display errors
      });
      return false;
    }
  });

  $('#change-password').click(event => {
    event.preventDefault();
    $('#password-modal form').form('clear');
    $('.ui.modal#password-modal').modal('show');
  });

  $('.ui.modal#delete-self-modal').modal({
    onApprove: function (event) {
      $('html').api({
        url: '/api/users/me',
        method: 'DELETE',
        on: 'now',
        onSuccess: () => {
          $().logout();
        }
      });
      return false;
    }
  });

  $('#delete-account').click(event => {
    event.preventDefault();
    $('.ui.modal#delete-self-modal').modal('show');
  });

  $('#edit-user-modal form').form({
      on: 'blur',
      inline: true,
      fields: {
        email: {
          identifier: 'email',
          rules: [
            {
              type: 'empty',
              prompt: 'Please enter an email'
            },
            {
              type: 'email',
              prompt: 'Please enter a valid email'
            }
          ]
        },
        newPasswordRepeat: {
          rules: [
            {
              type: 'match[newUserPassword]',
              prompt: 'The two passwords don\'t match'
            }
          ]
        }
      }
    });

  $('#edit-user-modal').modal({
    onApprove: function (event) {
      let form = $('#edit-user-modal form');
      form.form('validate form');
      if (!form.form('is valid')) return false;
      form.api({
        url: '/api/users/{id}',
        method: 'POST',
        on: 'now',
        beforeSend: function (settings) {
          settings.urlData.id = settings.data.id;
          settings.data.password = settings.data.newPassword;
          delete settings.data.newPassword;
          delete settings.data.newPasswordRepeat;
          return $.fn.api.settings.beforeSend(settings);
        },
        onSuccess: (response, element, xhr) => {
          $('#edit-user-modal').modal('hide');
          $().loadUsers();
        } //TODO display errors
      });
      return false;
    }
  });
});

