if ($().getAuthToken()) {
  $().redirectToDashboard();
}
$(document)
  .ready(() => {
    $('.ui.form')
      .form({
        on: 'blur',
        fields: {
          email: {
            identifier: 'email',
            rules: [
              {
                type: 'empty',
                prompt: 'Please enter your email'
              },
              {
                type: 'email',
                prompt: 'Please enter a valid email'
              }
            ]
          },
          password: {
            identifier: 'password',
            rules: [
              {
                type: 'empty',
                prompt: 'Please enter your password'
              }
            ]
          }
        }
      })
      .api({
        url: '/api/auth',
        method: 'POST',
        beforeXHR: xhr => xhr, // Replace our default xhr modification
        onSuccess: (response, element, xhr) => { // TODO display error messages
          if (response.token) {
            $().setAuthToken(response.token);
            $().redirectToDashboard();
          }
        }
      });
  });
