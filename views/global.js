$.fn.api.settings.contentType = "application/json; charset=utf-8";
$.fn.api.settings.serializeForm = true;
$.fn.api.settings.beforeSend = settings => {
  settings.data = JSON.stringify(settings.data);
  return settings;
};
$.fn.setAuthToken = token => {
  localStorage.setItem('token', token);
};
$.fn.getAuthToken = () => {
  return localStorage.getItem('token');
};
$.fn.deleteAuthToken = () => {
  localStorage.removeItem('token');
};
$.fn.logout = () => {
  $().deleteAuthToken();
  $().redirectToLogin();
};
$.fn.redirectToLogin = () => {
  window.location.href = './login';
};
$.fn.redirectToDashboard = () => {
  window.location.href = './dashboard';
};
$.fn.api.settings.beforeXHR = xhr => {
  const token = $().getAuthToken();
  if (token) xhr.setRequestHeader('Authorization', `Bearer ${token}`);
  else $().redirectToLogin();
  return xhr;
};
$.fn.form.settings.onSuccess = (event) => {
  if (event) event.preventDefault();
};
$.fn.convertToRetardedFormObject = object => { // Convert an object into the retarded format semantic ui needs to fill forms
  let retarded = {};
  const recursive = (prefix, subObject, first) => {
    if (subObject instanceof Object && !(subObject instanceof Array)) {
      Object.entries(subObject).forEach(([k, v]) => {
        recursive(first ? k : prefix + '[' + k + ']', v)
      })
    } else {
      retarded[prefix] = subObject
    }
  };
  recursive('', object, true);
  return retarded;
};
