FROM node:lts-alpine AS builder
LABEL maintainer="DerEnderKeks"

WORKDIR /usr/src/app

# bcrypt dependencies
RUN apk --no-cache add --virtual builds-deps build-base python
RUN apk --no-cache add git py-pip

COPY package*.json ./
COPY yarn.lock ./

RUN yarn install --production

COPY lib/funk ./lib/funk
RUN cd ./lib/funk && pip install -r requirements.txt

FROM node:lts-alpine

RUN apk --no-cache add python

WORKDIR /usr/src/app

COPY --from=builder /usr/src/app .
COPY . .

EXPOSE 80/tcp

CMD ["node", "./index.js"]
