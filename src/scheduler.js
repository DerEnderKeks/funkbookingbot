const db = require('./db/handler');
const funkAPI = require('../lib/funk');

let lastRun = null;

const PlanString = {
  1: 'Pause',
  2: '1GB',
  3: 'Unlimited'
};

const dayMap = {
  0: 'monday',
  1: 'tuesday',
  2: 'wednesday',
  3: 'thursday',
  4: 'friday',
  5: 'saturday',
  6: 'sunday'
};

const bookPlans = () => {

};

const getDelayUntilRun = () => {
  const hour = 1;
  const dailyTime = new Date();
  if (dailyTime.getHours() >= hour) dailyTime.setDate(dailyTime.getDate() + 1); // next day if past target hour
  dailyTime.setHours(hour);
  dailyTime.setMinutes(0);
  dailyTime.setSeconds(0);
  dailyTime.setMilliseconds(0);

  let diff = dailyTime - Date.now();
  return diff < 0 ? 0 : diff;
};

const schedule = () => {
  setTimeout(async () => {
    let dbStart = 0;
    const dbLimit = 100;
    let foundResults = true;

    let weekday = new Date().getDay();
    weekday = weekday === 6 ? 0 : weekday + 1; // offset to sane weekday order

    let promises = [];

    while (foundResults) {
      promises = [];
      const users = await db.getUsers(dbStart, dbLimit);
      if (users instanceof Error) {
        foundResults = false;
        continue;
      }
      users.forEach(user => {
        if (!user.funk || !user.funk.username || !user.funk.password) {
          console.log(`Scheduler: Skipping user '${user.email}'. Funk credentials incomplete.`);
          return;
        }
        if (!user.schedule || !user.schedule[dayMap[weekday]] || user.schedule[dayMap[weekday]] === 0) {
          console.log(`Scheduler: Skipping user '${user.email}'. No plan scheduled.`);
          return;
        }
        const api = new funkAPI(user.funk.username, user.funk.password);
        const scheduledPlanYesterday = user.schedule[dayMap[weekday === 0 ? 6 : weekday - 1]]; // useless? fetch actual plan instead?
        const scheduledPlanToday = user.schedule[dayMap[weekday]];
        const scheduledPlanTomorrow = user.schedule[dayMap[weekday === 6 ? 0 : weekday + 1]];
        let prom;
        switch (scheduledPlanToday) { // TODO fix - fetch active plan, decide if/what to book
          case 1:
            prom = api.order1GBPlan();
          case 2:
            prom = api.orderUnlimitedPlan();
          case 3:
            prom = api.startPause();
        }
        promises.push(prom);
        prom.then(() => {
          console.log(`Scheduler: Booked ${PlanString[scheduledPlan]} plan for user '${user.email}'.`);
        }).catch() // TODO
      });
      await Promise.all(promises);
    }
    schedule(); // repeat self
  }, getDelayUntilRun())
};

schedule(); // start infinite scheduling loop
