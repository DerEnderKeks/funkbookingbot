const fp = require('fastify-plugin');

const thinky = require('../db/db');
const models = require('../db/models');

async function dbConnector (fastify, options) {
  fastify.decorate('db', thinky);
  fastify.decorate('models', models);
}

module.exports = fp(dbConnector);
