const fp = require("fastify-plugin");
const db = require('../db/handler');

module.exports = fp(async (fastify, options) => {
  const secret = process.env.WEB_JWT_SECRET;
  if (!secret) {
    fastify.log.fatal('WEB_JWT_SECRET not set.');
    process.exit(1);
  }
  fastify.register(require("fastify-jwt"), {
    secret: secret
  });

  fastify.decorate("authenticate", async (request, reply) => {
    try {
      await request.jwtVerify()
    } catch (err) {
      reply.send(err)
    }
  });

  fastify.decorate("admin", async (request, reply) => {
    const isAdmin = await db.isUserAdmin(request.user.id);
    if (isAdmin !== true) {
      reply.unauthorized();
    }
  })
});
