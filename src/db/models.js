const thinky = require('./db');

exports.User = thinky.createModel("Users", {
  id: thinky.type.string(),
  email: thinky.type.string().required(),
  password: thinky.type.string().required(),
  isAdmin: thinky.type.boolean().default(false),
  funk: {
    username: thinky.type.string(),
    password: thinky.type.string(),
  },
  schedule: {
    monday: thinky.type.number(),
    tuesday: thinky.type.number(),
    wednesday: thinky.type.number(),
    thursday: thinky.type.number(),
    friday: thinky.type.number(),
    saturday: thinky.type.number(),
    sunday: thinky.type.number()
  }
});
