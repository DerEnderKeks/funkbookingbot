const thinky = require('../db/db');
const models = require('../db/models');
const encryption = require('../utils/encryption');

const Errors = {
  UserExists: new Error('User exists'),
  NotFound: new Error('Not found')
};
exports.Errors = Errors;

exports.isUserAdmin = (id) => new Promise(async (resolve, reject) => {
  console.log(id);
  models.User.get(id).pluck('isAdmin').execute().then(async res => {
    resolve(res.isAdmin);
  }).catch(thinky.Errors.DocumentNotFound, () => {
    resolve(null);
  }).error(reject);
});

exports.getUser = (id) => new Promise(async (resolve, reject) => {
  models.User.get(id).without({'funk': 'password'}).run().then(async res => {
    delete res.password;
    resolve(res);
  }).catch(thinky.Errors.DocumentNotFound, () => {
    resolve(null);
  }).error(reject);
});

exports.getFullUsers = (start = 0, limit = 25) => new Promise(async (resolve, reject) => {
  models.User.skip(start).limit(limit).run().then(async res => {
    resolve(res.map(user => {
      delete user.password;
      return user;
    }));
  }).error(reject);
});

exports.getUsers = (start = 0, limit = 25) => new Promise(async (resolve, reject) => {
  models.User.without({'funk': 'password', 'schedule': true}).skip(start).limit(limit).run().then(async res => {
    resolve(res.map(user => {
      delete user.password;
      return user;
    }));
  }).error(reject);
});

exports.getAdminUsers = () => new Promise(async (resolve, reject) => {
  models.User.filter({isAdmin: true}).run().then(async res => {
    if (res.length === 0) reject(Errors.NotFound);
    else resolve(res);
  }).error(reject);
});

exports.getUserByEmail = (email) => new Promise(async (resolve, reject) => {
  models.User.filter({email: email.toLowerCase()}).run().then(async res => {
    if (res.length === 0) reject(Errors.NotFound);
    else resolve(res[0]);
  }).error(reject);
});

exports.checkPassword = (id, password) => {
  return models.User.get(id).then((res) => {
    return encryption.comparePassword(password, res.password);
  });
};

exports.checkPasswordByEmail = (email, password) => {
  return exports.getUserByEmail(email).then((res) => {
    return encryption.comparePassword(password, res.password);
  });
};

exports.createUser = (user) => new Promise(async (resolve, reject) => {
  models.User.filter({email: user.email}).run().then(async res => {
    if (res.length > 0) return reject(Errors.UserExists);
    if (user.password) {
      user.password = await encryption.hashPassword(user.password);
    }
    models.User.save(user).then(resolve).error(reject);
  }).error(reject);
});

exports.editUser = (user) => new Promise(async (resolve, reject) => {
  models.User.get(user.id).run().then(async res => {
    if (user.password && user.password.length > 0) {
      user.password = await encryption.hashPassword(user.password);
    } else {
      delete user.password;
    }
    if (user.funk && user.funk.password && user.funk.password.length > 0) {
      user.funk.password = encryption.encryptPassword(user.funk.password);
    } else {
      delete user.funk.password;
    }
    models.User.get(user.id).update(user).run().then(res => {
      delete res.password;
      if (res.funk) delete res.funk.password;
      resolve(res);
    }).error(reject);
  }).catch(thinky.Errors.DocumentNotFound, () => {
    resolve(null);
  }).error(reject);
});

exports.deleteUser = (id) => new Promise(async (resolve, reject) => {
  models.User.get(id).run().then(async res => {
    res.delete().then(() => {
      resolve(true)
    }).error(reject);
  }).catch(thinky.Errors.DocumentNotFound, () => {
    resolve(null);
  }).error(reject);
});

exports.getFullUser = (id) => new Promise(async (resolve, reject) => {
  models.User.get(id).run().then(async res => {
    if (res.funk && res.funk.password) res.funk.password = encryption.decryptPassword(res.funk.password);
    resolve(res);
  }).catch(thinky.Errors.DocumentNotFound, () => {
    resolve(null);
  }).error(reject);
});

