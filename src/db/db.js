const thinky = require('thinky')({
  host: process.env.DB_HOST ? process.env.DB_HOST : 'localhost',
  port: process.env.DB_PORT ? process.env.DB_PORT : 28015,
  db: process.env.DB_DB ? process.env.DB_DB : 'db',
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
});

module.exports = thinky;
