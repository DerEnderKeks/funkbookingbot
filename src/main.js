const fastify = require('fastify')({
    logger: true
});
const resolve = require('path').resolve;

fastify.register(require('point-of-view'), {
  engine: {
    pug: require('pug')
  },
  templates: 'views',
  includeViewExtension: true,
  options: {
    filename: resolve('views')
  }
});

fastify.register(require('fastify-sensible'));

fastify.register(require('./plugins/db'));

fastify.register(require('./plugins/auth'));
fastify.register(require('./routes'));

_ = new Promise(() => {require('./setup')}); // Setup user in background

_ = new Promise(() => {require('./scheduler')}); // Funk scheduler in background

fastify.listen(process.env.WEB_PORT ? process.env.WEB_PORT : 80, process.env.WEB_HOST ? process.env.WEB_HOST: '0.0.0.0', (err, address) => {
    if (err) {
        fastify.log.error(err);
        process.exit(1);
    }
    fastify.log.info(`server listening on ${address}`)
});
