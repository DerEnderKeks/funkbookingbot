const db = require('./db/handler');

// TODO Schema validation for POST requests!!!
async function routes(fastify, options) {
  fastify.get('/', async (request, reply) => {
    reply.redirect('/login');
  });

  fastify.get('/login', async (request, reply) => {
    reply.view('login');
    await reply;
  });

  fastify.get('/dashboard', async (request, reply) => {
    reply.view('dashboard');
    await reply;
  });

  // Obligatory teapot endpoint
  fastify.get('/api/teapot', async (request, reply) => {
    reply.code(418);
    return {
      teapot: {
        text: 'I\'m a little teapot\nShort and stout\nHere is my handle\nHere is my spout\nWhen I get all steamed up\nHear me shout\nTip me over and pour me out!\n\nI\'m a very special teapot\nYes, it\'s true\nHere\'s an example of what I can do\nI can turn my handle into a spout\nTip me over and pour me out!',
        href: 'https://www.youtube.com/watch?v=e69-GO4bYLM'
      }
    }
  });

  fastify.post('/api/auth', async (request, reply) => {
    return db.checkPasswordByEmail(request.body.email, request.body.password).then((res) => {
        if (res) {
          return db.getUserByEmail(request.body.email).then((res) => {
            return reply.jwtSign({ //TODO expire token after 7 days
              id: res.id,
            }).then((res) => {
              return {token: res};
            })
          })
        } else {
          return fastify.httpErrors.unauthorized();
        }
      }
    ).catch(err => {
      if (err === db.Errors.NotFound) return fastify.httpErrors.unauthorized(); // TODO check if needed elsewhere (as not found)
      return err;
    })
  });

  fastify.get('/api/users/me', {
    preValidation: [fastify.authenticate]
  }, async (request, reply) => {
    return db.getUser(request.user.id).then(res => {
      if (!res) return fastify.httpErrors.unauthorized();
      return res;
    })
  });

  fastify.get('/api/users/:id', {
    preValidation: [fastify.authenticate, fastify.admin]
  }, async (request, reply) => {
    return db.getUser(request.params.id).then(res => {
      if (!res) return fastify.httpErrors.notFound();
      return res;
    })
  });

  fastify.post('/api/users/me', {
    preValidation: [fastify.authenticate]
  }, async (request, reply) => {
    if ('isAdmin' in request.body) {
      return fastify.httpErrors.forbidden('fuck off');
    } else {
      delete request.body.isAdmin; // Just to be sure
      request.body.id = request.user.id;
      return db.editUser(request.body);
    }
  });

  fastify.delete('/api/users/me', {
    preValidation: [fastify.authenticate]
  }, async (request, reply) => {
    return db.deleteUser(request.user.id).then(res => {
      if (res === true) return {success: true};
      return fastify.httpErrors.notFound();
    })
  });

  fastify.post('/api/users/me/password', {
    preValidation: [fastify.authenticate]
  }, async (request, reply) => {
    request.body.id = request.user.id;
    return db.checkPassword(request.body.id, request.body.old).then(res => {
      if (res !== true) return fastify.httpErrors.forbidden('Wrong password');
      return db.editUser({id: request.body.id, password: request.body.new});
    })
  });

  fastify.delete('/api/users/:id', {
    preValidation: [fastify.authenticate, fastify.admin]
  }, async (request, reply) => {
    return db.deleteUser(request.user.id).then(res => {
      if (res === true) return {success: true};
      return fastify.httpErrors.notFound();
    })
  });

  fastify.post('/api/users/:id', {
    preValidation: [fastify.authenticate, fastify.admin]
  }, async (request, reply) => {
    request.body.id = request.params.id;
    return db.editUser(request.body).then(res => { //TODO prevent empty password
      if (!res) return fastify.httpErrors.notFound();
      return res;
    })
  });

  fastify.get('/api/users', {
    preValidation: [fastify.authenticate, fastify.admin]
  }, async (request, reply) => {
    let start = Number.isInteger(Number(request.query.start)) ? Number(request.query.start) : 0;
    let limit = Number.isInteger(Number(request.query.limit)) ? Number(request.query.limit) : 25;
    return db.getUsers(start, limit);
  });
}

module.exports = routes;
