const db = require('./db/handler');

// Create default user when there is no admin
// The default user will not be recreated if it exists but is no admin
db.getAdminUsers().catch((err) => {
  if (err !== db.Errors.NotFound) throw err;
  db.createUser({
    email: 'admin@changeme.local',
    password: 'admin',
    isAdmin: true
  }).then(() => {
    console.log('Created default user \'admin@changeme.local\' (password \'admin\').')
  }).catch(err => {
    if (err === db.Errors.UserExists) {
      console.warn('WARNING: No admin users found, default user \'admin@changeme.local\' exists but is no admin.')
    } else {
      throw err;
    }
  })
});
