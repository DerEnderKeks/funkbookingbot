const util = require('util');
const execFile = util.promisify(require('child_process').execFile);

module.exports = class FunkAPI {
  constructor(username, password) {
    this.username = username;
    this.password = password;
  }

  // I am too lazy to implement the Funk API natively, so this has to suffice
  #callLib(call) {
    return new Promise((resolve, reject) => {
      execFile(path.join(__dirname, 'funk.py'), [this.username, this.password, call])
        .then(({stdout}) => {
          try {
            let parsed = JSON.parse(stdout);
            resolve(parsed);
          } catch (e) {
            return reject(e)
          }
        })
        .catch(reject)
    })
  }

  checkCredentials() {
    return this.#callLib('checkCredentials');
  }

  getData() {
    return this.#callLib('getData');
  }

  order1GBPlan() {
    return this.#callLib('order1GBPlan');
  }

  orderUnlimitedPlan() {
    return this.#callLib('orderUnlimitedPlan');
  }

  startPause() {
    return this.#callLib('startPause');
  }
};
