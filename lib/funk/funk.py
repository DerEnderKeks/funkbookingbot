#!/usr/bin/env python3

import os
import sys
import json

sys.path.append(os.path.join(sys.path[0], 'src', 'funkapi'))

from funkapi import FunkAPI
import botocore

def main():
  # Require 3 arguments: user, pass, function
  arguments = len(sys.argv) - 1
  if arguments != 3:
    print(json.dumps({'status': 'error', 'error': 'Too few arguments'}))
    exit(1)

  user = sys.argv[1]
  password = sys.argv[2]
  call = sys.argv[3]

  try:
    api = FunkAPI(user, password)
  except Exception as e:
    print(json.dumps({'status': 'error', 'error': e.__class__.__name__}))
    exit(1)

  if call == 'checkCredentials':
    # checked when the api is initialised
    exit(0)

  result = {}
  try:
    if call == 'getData':
      result = api.getData()
    elif call == 'order1GBPlan':
      result = api.order1GBPlan()
    elif call == 'orderUnlimitedPlan':
      result = api.orderUnlimitedPlan()
    elif call == 'startPause':
      result = api.startPause()
    else:
      print(json.dumps({'status': 'error', 'error': 'Invalid request'}))
      exit(1)
  except Exception as e:
    print(json.dumps({'status': 'error', 'error': e.__class__.__name__}))
    exit(1)
  result.update({"status": "success"})
  print(json.dumps(result)
  exit(0)

if __name__== "__main__":
  main()
